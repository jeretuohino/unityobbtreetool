﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBBData : MonoBehaviour
{
    public VoxelGroup voxelGroup;

    public bool debugDraw;
    [HideInInspector]public Vector3[] points;
    public Vector3 offset;
    public Transform relativeTransform;

    private void OnDrawGizmosSelected()
    {
        if (!debugDraw || points == null || offset == null || relativeTransform == null) return;
        foreach (Vector3 point in points)
        {
            //Gizmos.color = tp.Item2;
            //Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.localScale);
            //Gizmos.matrix = Matrix4x4.TRS(transform.position - offset, transform.rotation, transform.localScale);
            Gizmos.DrawSphere(point - offset, 0.005f);
        }
    }
}
