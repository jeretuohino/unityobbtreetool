﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathGeoLib;
using System.Linq;
using System;

[System.Serializable]
public class VoxelGroup
{
    public Vector3Int voxelcount;
    [HideInInspector]public List<Voxel> voxels;
    public VoxelGroup rootGroup;
    public VoxelGroup[] childGroups = new VoxelGroup[2];
    public string splitName = "";

    public VoxelGroup(List<Voxel> _voxels, Vector3Int _voxelcount, string split = "root", VoxelGroup root = null)
    {
        voxelcount = _voxelcount;
        voxels = _voxels;
        rootGroup = root;
        splitName = split;
    }

    float? GetSplittingPoint(List<Tuple<int,int>> longestAxises, float spliceThreshold)
    {
        #region OLD UNFINISHED METHOD
        //int? shortestSpot = null;
        //int? depth = null;
        //List<int> depthList = new List<int>();

        //// Follow along the longest Axis
        //for (int w = 0; w < voxelcount[axisInt[0]]; w++)
        //{
        //    int? currentDepth = null;
        //    // Follow the 2nd longest Axis for 
        //    // Trace for the first Voxel, then trace the depth of it
        //    // If we hit multiple voxel lines along axis, save the shortest one
        //    for (int h = 0; h < voxelcount[axisInt[1]]; h++)
        //    {
        //        for (int d = 0; d < voxelcount[axisInt[2]]; d++)
        //        {

        //        }
        //        // TODO
        //        // Identify a big variation between 2 voxel rows
        //        // This is the splitting point
        //        if (currentDepth == null || currentDepth < depth)
        //        {

        //        }
        //    }

        //    // Save the thinnest part from this Axis
        //    /*
        //     * T1 = min f (x) / max f (x). 
        //     */
        //}
        #endregion

        int axisStartPoint = voxels.Select(v => v.PositionInGrid()[longestAxises[0].Item1]).Min();
        int[] vcasp = new int[longestAxises[0].Item2]; // Voxel Count Along Splitting Plane
        Debug.LogFormat("Starting SplittingpointCalc\n\tvcasp lenght = {0}\n\tlongestAxis = {1}\n\tsplitName = {2}", vcasp.Length, string.Join((","), longestAxises), splitName);

        // Count all group on the position of a potential splitting plane
        foreach (Voxel v in voxels)
        {
            //Debug.Log("++");
            // Increment
            try
            {
                Vector3Int gridPos = v.PositionInGrid();
                //Debug.Log(Mathf.Max(0, gridPos[longestAxises[0].Item1]));
                // Is within Array range
                if( gridPos[longestAxises[0].Item1] - axisStartPoint > -1 && 
                    gridPos[longestAxises[0].Item1] - axisStartPoint < vcasp.Length)
                {
                    vcasp[gridPos[longestAxises[0].Item1] - axisStartPoint] += 1;
                    //Debug.Log("\t" + gridPos[longestAxises[0].Item1] + " - " + axisStartPoint + " = " + (gridPos[longestAxises[0].Item1] - axisStartPoint).ToString());
                }

            } catch (IndexOutOfRangeException ex)
            {
                Debug.LogErrorFormat("ERROR INCREMENTING\n\tvcasp lenght = {0}" +
                    "\n\tlongestAxis = {1}" +
                    "\n\tv.PositionInGrid() = {2}" +
                    "\n\taxisStartPoint = {3}", 
                    vcasp.Length, longestAxises[0], v.PositionInGrid(), axisStartPoint);
                return null;
            }
        }

        /*
         f(x) = the minimum cross section area function
        To locate the place of the biggest jump, we 
        first compute the interlaced local minimum and maximum points of f(x). 
        Suppose one of the local minimum point is xmin,
        and the local maximum point near to it is xmax. 
        Then, the jump value at xmin is Jxmin = f (xmax) − f (xmin). By simple
        comparison, the biggest jump can be located, supposing it is
        Jx bmin = f (xbmax) − f (xbmin). 
        Then, the current OBB will be sliced into two sub-OBBs at x = xbmin.
        */
        Debug.Log("[" + string.Join(", ", vcasp) + "]");

        // Check if model varies a lot at some point
        if ((float)vcasp.Min() / (float)vcasp.Max() < spliceThreshold)
        {      
            // Under threshold, find most fitting splicing point
            // Jxmin = f(xmax) - f(xmin)
            // xmin = One of the Local minimum point
            // xmax = local maximum point near xmin
            float biggestJump = 0;
            int biggestJumpPointMax = -1;
            int biggestJumpPointMin = -1;

            for (int i = 1; i < vcasp.Length - 1; i++)
            {
                if (vcasp[i] == 0) 
                {
                    Debug.LogWarning("We have an empty row here. Something is wrong");
                    //OBBDebug.Instance.SetDraw(voxels);
                    //OBBDebug.Instance.drawAxis = longestAxises[0].Item1;
                    //return null;
                }
                // Get the nearest maximum
                int xmaxInteger = (vcasp[i - 1] > vcasp[i + 1]) ? i-1 : i+1;
                if(vcasp[xmaxInteger] - vcasp[i] > biggestJump)
                {
                    biggestJump = vcasp[xmaxInteger] - vcasp[i];
                    biggestJumpPointMax = xmaxInteger;
                    biggestJumpPointMin = i;
                }
            }

            //if (biggestJumpPointMin < 0 && biggestJumpPointMax > -1) return voxels.First(v => v.PositionInGrid()[longestAxises[0].Item1] - axisStartPoint == biggestJumpPointMax).pos[longestAxises[0].Item1];
            //if (biggestJumpPointMin > -1 && biggestJumpPointMax < 0) return voxels.First(v => v.PositionInGrid()[longestAxises[0].Item1] - axisStartPoint == biggestJumpPointMin).pos[longestAxises[0].Item1];
            //if (biggestJumpPointMin < 0 && biggestJumpPointMax < 0) return null;

            // Calculate the splice point between min and max biggsetJumpPoint
            Voxel minV = voxels.FirstOrDefault(v => v.PositionInGrid()[longestAxises[0].Item1] - axisStartPoint == biggestJumpPointMin);
            Voxel maxV = voxels.FirstOrDefault(v => v.PositionInGrid()[longestAxises[0].Item1] - axisStartPoint == biggestJumpPointMax);
            if (minV != null && maxV != null)
            {
                //if (exampleV == null) exampleV = voxels.First(v => v.PositionInGrid()[longestAxises[0].Item1] - axisStartPoint == biggestJumpPointMax);
                return (minV.pos[longestAxises[0].Item1] + maxV.pos[longestAxises[0].Item1]) / 2;
            }
            else
            {
                //if (minV != null) return minV.pos[longestAxises[0].Item1];
                //if (maxV != null) return maxV.pos[longestAxises[0].Item1];
                //Debug.LogError("Couldn't find an appropriate splitting point to return");
                return null;
            }
        }

        // Over Threshold, ignoring
        Debug.LogWarning("Not splitting since object is over threshold\n\t" + spliceThreshold + " > " + (float)vcasp.Min() / (float)vcasp.Max() + "\n" + vcasp.Min() + " / " + vcasp.Max());
        return null;
    }

    public bool TrySplit(float threshold)
    {
        Debug.Log("Trying to split " + voxels.Count + " voxels");

        // Get the longest axis
        List<Tuple<int,int>> longestAxis = new List<Tuple<int,int>>(){ 
            new Tuple<int,int>(0, voxelcount.x),
            new Tuple<int,int>(1, voxelcount.y),
            new Tuple<int,int>(2, voxelcount.z) };
        longestAxis.Sort((a, b) => b.Item2.CompareTo(a.Item2));

        // Get the splitting point, if any
        float? splittingPoint = GetSplittingPoint(longestAxis, threshold);

        if(splittingPoint != null)
        { 
            // Split along the longest axis and get the + and - groups
            List<Voxel> aVoxels = voxels.FindAll(v => v.pos[longestAxis[0].Item1] <= splittingPoint);
            List<Voxel> bVoxels = voxels.FindAll(v => v.pos[longestAxis[0].Item1] >= splittingPoint);

            Vector3Int aVoxelcount = new Vector3Int(0,0,0);
            Vector3Int bVoxelcount = new Vector3Int(0,0,0);

            // Calculate the new min max for the voxels
            for (int i = 0; i < 3; i++)
            {
                int amax = aVoxels.Select(v => v.PositionInGrid()[longestAxis[i].Item1]).Max();
                int bmax = bVoxels.Select(v => v.PositionInGrid()[longestAxis[i].Item1]).Max();
                aVoxelcount[longestAxis[i].Item1] = amax - aVoxels.Select(v => v.PositionInGrid()[longestAxis[i].Item1]).Min() + 1;
                bVoxelcount[longestAxis[i].Item1] = bmax - bVoxels.Select(v => v.PositionInGrid()[longestAxis[i].Item1]).Min() + 1;
            }

            List<VoxelGroup> split = new List<VoxelGroup>();
            if (aVoxels.Count > 0) childGroups[0] = new VoxelGroup(aVoxels, aVoxelcount, splitName + "a", this);
            if (bVoxels.Count > 0) childGroups[1] = new VoxelGroup(bVoxels, bVoxelcount, splitName + "b", this);

            Debug.Log("original => " + voxelcount + "\naSplit => " + aVoxelcount + "\nbSplit => " + bVoxelcount);

            //childGroups = split.ToArray();
            return true;
        }

        return false;
    }
}

[System.Serializable]
public class Voxel
{
    public float size;
    public Vector3 cubeSize { get { return new Vector3(size, size, size); } }
    public Vector3 pos;
    public Vector3 offsetFromCenter;
    public Color drawColor = Color.white;
    public List<Vector3> faceNormals = new List<Vector3>();

    private List<Tuple<Vector3, Vector3>> intersects = new List<Tuple<Vector3, Vector3>>();

    public Voxel(float _size, Vector3 _pos, Vector3 _offsetFromCenter)
    {
        size = _size;
        pos = _pos - cubeSize/2;
        offsetFromCenter = _offsetFromCenter;
    }
    
    public void DrawVoxelGizmos(Vector3 offset, Vector3[] unitVectors, Vector3 scale, Color? color = null)
    {
        Gizmos.color = (color == null) ? drawColor : (Color)color;
        Gizmos.matrix = Matrix4x4.TRS(offset + offsetFromCenter, Quaternion.LookRotation(unitVectors[0], unitVectors[1]), scale);
        Gizmos.DrawWireCube(pos, cubeSize);
        //Gizmos.DrawLine(pos, voxelNormal*2);
    }    
    public void DrawVoxelGizmos(Vector3 offset, Quaternion rotation, Vector3 scale, Color? color = null)
    {
        Gizmos.color = (color == null) ? drawColor : (Color)color;
        Gizmos.matrix = Matrix4x4.TRS(offset, rotation, scale);
        Gizmos.DrawWireCube(pos + offsetFromCenter, cubeSize);
        //Gizmos.DrawRay(pos + offsetFromCenter, voxelNormal * .25f);
    }

    public void DrawNormals(Vector3 offset, Vector3[] unitVectors, Vector3 scale, Color? color = null)
    {
        Gizmos.color = (color == null) ? drawColor : (Color)color;
        Gizmos.matrix = Matrix4x4.TRS(offset + offsetFromCenter, Quaternion.LookRotation(unitVectors[0], unitVectors[1]), scale);
        Gizmos.DrawLine(pos, GetNormal()*2);
    }
    public void DrawNormals(Vector3 offset, Quaternion rotation, Vector3 scale, float raylength, Color? color = null)
    {
        Gizmos.color = (color == null) ? drawColor : (Color)color;
        Gizmos.matrix = Matrix4x4.TRS(offset, rotation, scale);
        Gizmos.DrawRay(pos + offsetFromCenter, GetNormal() * raylength);
    }

    public Vector3 GetNormal()
    {
        if (faceNormals.Count < 1) return Vector3.zero;

        return faceNormals.Aggregate(new Vector3(0, 0, 0), (s, v) => s + v) / faceNormals.Count();
    }
    //public void DrawIntersectGizmos(Vector3 offset, Vector3[] unitVectors, Vector3 scale)
    //{
    //    Gizmos.matrix = Matrix4x4.TRS(offset, Quaternion.LookRotation(unitVectors[0], unitVectors[1]), scale);
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawWireCube(pos, new Vector3(minMax.Item2.x - minMax.Item1.x, minMax.Item2.y - minMax.Item1.y, minMax.Item2.z - minMax.Item1.z) * 0.9f);
    //    foreach (Tuple<Vector3, Vector3> i in intersects)
    //    {
    //        Vector3 center = (i.Item1 + i.Item2) / 2;
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawWireCube(center, new Vector3(Mathf.Abs(i.Item2.x - i.Item1.x), 
    //                                                Mathf.Abs(i.Item2.y - i.Item1.y), 
    //                                                Mathf.Abs(i.Item2.z - i.Item1.z)));
    //    }
    //}

    public bool Intersects(Vector3 bMin, Vector3 bMax)
    {
        Vector3 aMin = pos + offsetFromCenter - cubeSize / 2;
        Vector3 aMax = pos + offsetFromCenter + cubeSize / 2;

        if ((aMin.x <= bMax.x && aMax.x >= bMin.x) &&
            (aMin.y <= bMax.y && aMax.y >= bMin.y) &&
            (aMin.z <= bMax.z && aMax.z >= bMin.z))
        {
            intersects.Add(new Tuple<Vector3, Vector3>(bMin, bMax));
            //intersects.Add(new Tuple<Vector3, Vector3>(aMin, aMax));
            return true;
        }
        return false;
    }

    public Vector3 RelativePos()
    {
        return pos + offsetFromCenter;
    }

    public Vector3Int PositionInGrid()
    {
        // Because the pos is centered, substract the cube extend so the pos is 0,0,0
        // Divide by the size so we get the multiplier count
        return Vector3Int.RoundToInt((pos - cubeSize / 2) / size);
    }
}

static public class OBBVoxel
{
    static public List<Tuple<Vector3, Color>> testPoints = new List<Tuple<Vector3, Color>>();

    static public List<Voxel> VoxelizeOBB(OrientedBoundingBox obb, float voxelsize)
    {
        // Get basic 3D box of voxels
        Vector3 size = obb.Extent * 2;

        List<Voxel> voxels = new List<Voxel>();

        for (int z = 0; z <= Mathf.CeilToInt(size.z / voxelsize); z++)
        {
            for (int y = 0; y <= Mathf.CeilToInt(size.y / voxelsize); y++)
            {
                for (int x = 0; x <= Mathf.CeilToInt(size.x / voxelsize); x++)
                {
                    //voxels[x, y, z] = new Voxel(voxelsize, new Vector3(x, y, z));
                    voxels.Add(new Voxel(voxelsize, new Vector3(x, y, z) * voxelsize, obb.CornerPoint(0) - obb.Center));
                }
            }
        }

        return voxels;
    }

    /*  THIS NEEDS HEAVY OPTIMIZATIONS, PROB ANNOYING LOOP SOMEWHERE */
    static public List<Voxel> VoxelToMeshShell(Mesh mesh, List<Voxel> voxelsToTrim, Vector3 pivotOffset, Quaternion rotation)
    {
        //// Trim the voxels outside of the shell
        List<Voxel> solidVoxels = new List<Voxel>();

        //For every triangle
        for(int i = 0; i < mesh.triangles.Length - 2; i += 3)
        {
            Vector3[] tri = new Vector3[3] { 
                mesh.vertices[mesh.triangles[i]], 
                mesh.vertices[mesh.triangles[i+ 1]], 
                mesh.vertices[mesh.triangles[i+ 2]] };

            // Get Tris BoundingBox
            Vector3 min = tri[0];
            Vector3 max = tri[0];
            for (int v = 1; v < 3; v++)
            {
                min.x = Mathf.Min(tri[v].x, min.x);
                min.y = Mathf.Min(tri[v].y, min.y);
                min.z = Mathf.Min(tri[v].z, min.z);

                max.x = Mathf.Max(tri[v].x, max.x);
                max.y = Mathf.Max(tri[v].y, max.y);
                max.z = Mathf.Max(tri[v].z, max.z);
            }

            // Bubblegum
            //pivotOffset = Vector3.zero;

            //check every voxel in the triangle's bounding box 
            for (int vi = voxelsToTrim.Count - 1; vi >= 0; vi--)
            {
                //see if it intersects
                //if (voxelsToTrim[vi].Intersects(min, max))
                if (OBBUtils.TriangleIntersectsBox(tri[0] - pivotOffset, tri[1] - pivotOffset, tri[2] - pivotOffset, rotation * voxelsToTrim[vi].RelativePos(), voxelsToTrim[vi].cubeSize / 2))
                {
                    //if (!OBBUtils.TriangleIntersectsBox(tri[0] - pivotOffset, tri[1] - pivotOffset, tri[2] - pivotOffset, voxelsToTrim[vi].RelativePos(), voxelsToTrim[vi].cubeSize / 2)) continue;
                    //If it does, the voxel is made solid

                    //center = ((P1 + P2 + P3) / 3)

                    //P1 = normals[triangles[triangle * 3]]
                    //P2 = normals[triangles[triangle * 3 + 1]]
                    //P3 = normals[triangles[triangle * 3 + 2]]

                    Vector3 faceNormal = ((mesh.normals[mesh.triangles[i]] + mesh.normals[mesh.triangles[i+1]] + mesh.normals[mesh.triangles[i+2]]) / 3);
                    voxelsToTrim[vi].faceNormals.Add(faceNormal);
                    solidVoxels.Add(voxelsToTrim[vi]);
                    voxelsToTrim.RemoveAt(vi);
                    voxelsToTrim.TrimExcess();
                }
            }
        }

        return solidVoxels;
    }

    static public VoxelGroup FillVoxelMeshShell(List<Voxel> shell, Vector3 obbsize)
    {
        Vector3Int voxelCount = Vector3Int.zero;
        float voxelSize = shell[0].size;

        for (float z = voxelSize / 2; z < obbsize.z; z += voxelSize)
        {
            for (float y = voxelSize / 2; y < obbsize.y; y += voxelSize)
            {
                bool penDown = false;
                int lastVoxel = -1;
                List<Voxel> newVoxels = new List<Voxel>();

                // Follow along X axis
                for (float x = voxelSize / 2; x < obbsize.x; x += voxelSize)
                {
                    int voxi = shell.FindIndex(v => v.pos == new Vector3(x,y,z));

                    if (voxi > -1)
                    {
                        // Get the normal of the Triangle in detected voxel
                        // If the normal is against the movement dir (-X), lower pen
                        // If the normal is along the movement dir (+X), raise pen
                        penDown = !(Vector3.Dot(shell[voxi].GetNormal(), new Vector3(1, 0, 0)) > 0) && (Vector3.Dot(shell[voxi].GetNormal(), new Vector3(1, 0, 0)) < 0);

                        lastVoxel = voxi;
                        testPoints.Add(new Tuple<Vector3, Color>(new Vector3(x, y, z), Color.red));
                    }
                    else if (voxi < 0 && penDown)
                    {
                        Voxel newVox = new Voxel(
                                        shell[0].size,
                                        new Vector3(x, y, z) + shell[0].cubeSize / 2,
                                        shell[0].offsetFromCenter);
                        newVox.drawColor = Color.red;
                        newVoxels.Add(newVox);
                        //Debug.Log("New voxel added at " + lastVoxel);
                        testPoints.Add(new Tuple<Vector3, Color>(new Vector3(x, y, z), Color.green));
                    }
                    else
                    {
                        testPoints.Add(new Tuple<Vector3, Color>(new Vector3(x, y, z), Color.gray));
                    }
                }

                if (newVoxels.Count > 0)
                {
                    shell.InsertRange(lastVoxel, newVoxels);
                    newVoxels.Clear();
                }
            }
        }

        voxelCount = new Vector3Int(shell.Select(v => v.PositionInGrid().x).Max() + 1,
                                    shell.Select(v => v.PositionInGrid().y).Max() + 1,
                                    shell.Select(v => v.PositionInGrid().z).Max() + 1);

        VoxelGroup group = new VoxelGroup(shell, voxelCount);
        Debug.Log("Created group with voxelcount of " + voxelCount);

        return group;
    }
}
