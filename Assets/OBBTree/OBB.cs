﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathGeoLib;

public class OBB
{
    static public OrientedBoundingBox GetOBB(Vector3[] points, Transform meshTransform)
    {
        //OrientedBoundingBox obb = new OrientedBoundingBox(meshTransform.TransformPoint(points[0]), Vector3.zero, meshTransform.right, meshTransform.up, meshTransform.forward);
        OrientedBoundingBox obb = new OrientedBoundingBox(points[0], Vector3.zero, Vector3.right, Vector3.up, Vector3.forward);

        for (int i = 1; i < points.Length; i++)
        {
            obb.Enclose(points[i]);
            //obb.Enclose(meshTransform.TransformPoint(points[i]));
        }

        return obb;
    }

    static public GameObject GenerateOBBCollider(OrientedBoundingBox obb)
    {
        GameObject obbGo = new GameObject();

        BoxCollider col = obbGo.gameObject.AddComponent<BoxCollider>();
        obbGo.transform.rotation = Quaternion.LookRotation(obb.Axis3, obb.Axis2);
        col.size = obb.Extent * 2;
        obbGo.transform.position = obb.Center;
        //col.center = obb.Center;

        return obbGo;
    }
}
