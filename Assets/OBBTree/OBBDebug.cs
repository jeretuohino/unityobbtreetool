﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using MathGeoLib;

//public class OBBDebug : MonoBehaviour
//{
//    private static OBBDebug _instance;

//    public static OBBDebug Instance { get { return GetInstance(); } }

//    static OBBDebug GetInstance()
//    {
//        if(_instance == null)
//        {
//            _instance = FindObjectOfType<OBBDebug>();
//            if(_instance == null)
//            {
//                GameObject go = new GameObject();
//                go.AddComponent<OBBDebug>();
//            }
//        }
//        return _instance;
//    }

//    List<Voxel> voxelsToDraw;
//    OrientedBoundingBox obb;
//    Vector3 scale;
//    Vector3 pos;
//    Quaternion rot;
//    public int drawAxis;

//    [ContextMenu("Reset")]
//    public void Reset()
//    {
//        voxelsToDraw.Clear();        
//    }

//    public void OnDrawGizmos()
//    {
//        foreach (Voxel v in voxelsToDraw)
//        {
//            //v.DrawVoxelGizmos(obb.Center + transform.position, new[] { obb.Axis3, obb.Axis2, obb.Axis1 }, meshFilter.transform.localScale);
//            v.DrawVoxelGizmos(pos, rot, scale);
//            //v.DrawIntersectGizmos(obb.Center, new[] { obb.Axis3, obb.Axis2, obb.Axis1 }, meshFilter.transform.localScale);
//        }
//    }

//    public void SetDraw(List<Voxel> voxels)
//    {
//        voxelsToDraw = voxels;
//    }
//    public void SetTransforms(Vector3 p, Quaternion r, Vector3 s)
//    {
//        //obb.Center + transform.position, Quaternion.LookRotation(obb.Axis3, obb.Axis2) * transform.rotation, meshFilter.transform.localScale
//        pos = p;
//        rot = r;
//        scale = s;
//    }
//}
