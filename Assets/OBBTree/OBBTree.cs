﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Unity.Collections;
using MathGeoLib;
using System;

public class OBBTree : MonoBehaviour
{
    public MeshFilter meshFilter;
    public float minVoxelSize = 0.05f;

    private OrientedBoundingBox obb;
    [SerializeField] private List<VoxelGroup> voxelgroups = new List<VoxelGroup>();

    [Range(0,1)]
    [SerializeField] float jumpThreshold = 0.75f;
    [SerializeField] int maxspliceloops = 5;

    [Header("Debugs")]

    [SerializeField] bool debugDraw = false;
    [SerializeField] bool drawBoxes = false;
    [SerializeField] bool drawRows = false;
    [SerializeField] bool drawNormals = false;
    [SerializeField] float raylength = 0.05f;
    [Range(0,2)]
    [SerializeField] int drawAxis = 0;
    [SerializeField] bool drawDots = false;
    [SerializeField] int drawRow = 0;

    private void OnDrawGizmosSelected()
    {
        if (!debugDraw) return;
        if (meshFilter == null) GetComponent<MeshFilter>();
        if (meshFilter == null) return;
        if (obb == null || ((obb.Axis3 + obb.Axis2 + obb.Axis1) / 3).magnitude == 0) obb = OBB.GetOBB(meshFilter.sharedMesh.vertices, transform);

        if (voxelgroups!= null && voxelgroups.Count > 0)
        {
            //foreach (VoxelGroup vg in voxelgroups)
            for (int i = 0; i < voxelgroups.Count; i++)
            {
                VoxelGroup vg = voxelgroups[i];
                if (vg.voxels == null || vg.voxels.Count < 0) continue;
                foreach (Voxel v in vg.voxels)
                {   
                    if (drawRows && !(v.pos[drawAxis] - minVoxelSize * drawRow < minVoxelSize && v.pos[drawAxis] - minVoxelSize * drawRow > 0)) continue;
                    //v.DrawVoxelGizmos(obb.Center + transform.position, new[] { obb.Axis3, obb.Axis2, obb.Axis1 }, meshFilter.transform.localScale);
                    if(drawBoxes)v.DrawVoxelGizmos(obb.Center + transform.position, Quaternion.LookRotation(obb.Axis3, obb.Axis2) * transform.rotation, meshFilter.transform.localScale, OBBUtils.GetRainbowLerpColor(i,voxelgroups.Count()));
                    if(drawNormals)v.DrawNormals(obb.Center + transform.position, Quaternion.LookRotation(obb.Axis3, obb.Axis2) * transform.rotation, meshFilter.transform.localScale, raylength);
                    //v.DrawIntersectGizmos(obb.Center, new[] { obb.Axis3, obb.Axis2, obb.Axis1 }, meshFilter.transform.localScale);
                }

                // Draw Testpoint dots
                if (!drawDots) continue;
                foreach (Tuple<Vector3, Color> tp in OBBVoxel.testPoints)
                {
                    Gizmos.color = tp.Item2;
                    Gizmos.matrix = Matrix4x4.TRS(obb.CornerPoint(0) + transform.position, Quaternion.LookRotation(obb.Axis3, obb.Axis2) * transform.rotation, meshFilter.transform.localScale);
                    Gizmos.DrawSphere(tp.Item1, 0.005f);
                }
            }
        }
    }

    [ContextMenu("GenerateOBBCollider")]
    public void GenerateColliderForThis()
    {
        float startTime = Time.realtimeSinceStartup;
        float timeTaken;
        GameObject emptyChild = Clear();
        

        // Generate new GameObject with the OBB
        Mesh ms = meshFilter.sharedMesh;
        Vector3[] verts = ms.vertices;

        obb = OBB.GetOBB(verts, transform);
        //GameObject obbgo = OBB.GenerateOBBCollider(obb);

        // Apply final rotations and positions
        //// NOTE[14/09]: This doesn't work with multiple colliders as of yet 
        emptyChild.transform.position = meshFilter.transform.position + obb.Center - obb.CornerPoint(0);
        emptyChild.transform.rotation = transform.rotation;
        emptyChild.transform.localScale = transform.localScale;

        //// Transform and hierarchy cleanup
        ///
        //obbgo.name = string.Format("OBB_{0}", emptyChild.childCount);
        //obbgo.transform.parent = emptyChild;
        //obbgo.transform.localPosition = Vector3.zero;

        timeTaken = Time.realtimeSinceStartup - startTime;
        startTime = Time.realtimeSinceStartup;

        //OBBDebug.Instance.SetTransforms(obb.Center + transform.position, Quaternion.LookRotation(obb.Axis3, obb.Axis2) * transform.rotation, meshFilter.transform.localScale);        //OBBDebug.Instance.SetTransforms(obb.Center + transform.position, Quaternion.LookRotation(obb.Axis3, obb.Axis2) * transform.rotation, meshFilter.transform.localScale);

        // Voxelise to find mesh shape
        //OBB Collider took: 0.001953125s
        Debug.Log("OBB Collider took: " + timeTaken + "s");

        List<Voxel> voxels = OBBVoxel.VoxelizeOBB(obb, minVoxelSize);

        timeTaken = Time.realtimeSinceStartup - startTime;
        startTime = Time.realtimeSinceStartup;

        // OBB Voxelization took: 0.05371094s
        Debug.Log("OBB Voxelization took: " + timeTaken + "s");

        voxels = OBBVoxel.VoxelToMeshShell(
                            meshFilter.sharedMesh,
                            voxels,
                            obb.Center,
                            Quaternion.LookRotation(obb.Axis3, obb.Axis2));

        timeTaken = Time.realtimeSinceStartup - startTime;
        startTime = Time.realtimeSinceStartup;

        /*  THIS NEEDS HEAVY OPTIMIZATIONS, PROB ANNOYING LOOP SOMEWHERE */
        //VoxelToMeshShell took: 68.57129s
        Debug.Log("VoxelToMeshShell took: " + timeTaken + "s");

        VoxelGroup baseVoxelGroup = OBBVoxel.FillVoxelMeshShell(voxels, obb.Extent * 2);

        timeTaken = Time.realtimeSinceStartup - startTime;
        startTime = Time.realtimeSinceStartup;

        /*  THIS COULD TAKE SOME OPTIMIZATIONS HERE  */
        // FillingVoxelMeshShell took: 18.44336s
        Debug.Log("FillingVoxelMeshShell took: " + timeTaken + "s");

        // Purify the VoxelGroup till we can't divide it anymore
        List<VoxelGroup> dirtyGroups = new List<VoxelGroup>() { baseVoxelGroup };
        //List<VoxelGroup> pureGroups = new List<VoxelGroup>();
        int loops = 0;

        while (dirtyGroups.Count > 0 && loops < maxspliceloops)
        {
            Debug.Log("Starting dirty splitting");
            loops++;

            // Purify the voxel group further
            VoxelGroup vg = dirtyGroups[0];

            // Pop the dirty group out of the list
            dirtyGroups.Remove(vg);
                
            // Try to Split the group
            if(vg.TrySplit(jumpThreshold))
            {
                // If succeed, add the dirty child groups into the list
                dirtyGroups.AddRange(vg.childGroups);
            }
            else
            {
                // Else, add to the PureGroups list and continue purifying the rest
                //Debug.Log("Added a vg to puregroups");
                //pureGroups.Add(vg);
            }
        }

        // Make colliders for the pure Voxel Groups
        //voxelgroups.Clear();

        // 1. Take one childgroup
        // 2. Create gameobject for it
        // 3. Set OBBCollider data to it
        // Return to 1 and terminate 

        List<VoxelGroup> queue = new List<VoxelGroup>() { baseVoxelGroup};
        List<VoxelGroup> pureGroups = new List<VoxelGroup>();

        //voxelgroups = pureGroups;
        while (queue.Count > 0)
        {
            // Pop first VoxelGroup out
            VoxelGroup vg = queue[0];
            queue.RemoveAt(0);
            // Find if parent transform exists
            Transform parentgo = null;
            if (vg.rootGroup != null) parentgo = gameObject.GetComponentsInChildren<Transform>().FirstOrDefault(t => t.name == vg.rootGroup.splitName);
            if (parentgo == null) parentgo = emptyChild.transform;

            // Make a Gameobject for this OBB
            Vector3[] points = vg.voxels.Where(v => v.GetNormal() != null).Select(v => v.pos).ToArray();
            OrientedBoundingBox newobb = OBB.GetOBB(points, transform);

            GameObject newobbgo;

            // Generate Collider for each that is considered "pure"
            if (vg.childGroups.All(v => v == null))
            {
                newobbgo = OBB.GenerateOBBCollider(newobb);
            }
            else
            {
                newobbgo = new GameObject();
                queue.AddRange(vg.childGroups.Where(cg => cg != null));
            }
            newobbgo.name = (vg.splitName != "") ? vg.splitName : "root";
            newobbgo.transform.parent = parentgo;
            newobbgo.transform.localPosition -= obb.Center - obb.CornerPoint(0);

            // Set the data object for debug
            OBBData data = newobbgo.AddComponent<OBBData>();
            data.offset = obb.Center - obb.CornerPoint(0);
            data.relativeTransform = emptyChild.transform;
            data.points = points;
            data.voxelGroup = vg;
            //newobbgo.transform.localPosition = Vector3.zero;
        }

        emptyChild.transform.position += meshFilter.transform.position;
        pureGroups.Add(baseVoxelGroup);
        //voxelgroups.ForEach(vg => vg.voxels.ForEach(v => v.PositionInGrid()));
    }

    [ContextMenu("ClearOBB")]
    public GameObject Clear()
    {
        // Clear previous data
        Transform emptyChildTransform = transform.Find("OBBs");
        if (emptyChildTransform == null)
        {
            GameObject emptyChild = new GameObject("OBBs");
            emptyChild.transform.parent = transform;
            emptyChild.transform.localPosition = Vector3.zero;
            return emptyChild;
        }
        else
        {
            int childcount = emptyChildTransform.childCount;
            for (var i = childcount - 1; i >= 0; i--)
            {
                DestroyImmediate(emptyChildTransform.GetChild(i).gameObject);
            }
            if (voxelgroups != null) voxelgroups.Clear();
            if(obb != null) obb = null;
            return emptyChildTransform.gameObject;
        }

    }
}
