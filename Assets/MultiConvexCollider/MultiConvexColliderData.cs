﻿using System.Collections.Generic;
using UnityEngine;
public class HalfEdge
{
    public HalfEdge neighbor;
    public Facet facet;
    public Vertex vertex;

    public HalfEdge(Facet facetPtr, Vertex vertexPtr)
    {
        facet = facetPtr;
        vertex = vertexPtr;
    }
}

public class Vertex
{
    public Vector3 pos;
    public int index;
    public Vertex(Vector3 _pos, int _index)
    {
        pos = _pos;
        index = _index;
    }
}

public class Facet
{
    public int index;
    Vertex[] verts;
}
