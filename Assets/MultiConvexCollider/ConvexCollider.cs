﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Convex
{

    bool Collide(Convex a, Convex b)
    {
        // TODO: Implement Seperating Axis Theorem collision check
        return false;
    }
}

public class ConvexCollider : MonoBehaviour
{
    [SerializeField]
    MeshRenderer mrenderer;
    [SerializeField]
    MeshFilter mfilter;

    private void OnEnable()
    {
        if (mrenderer == null) GetComponent<MeshRenderer>();
        if (mfilter == null) GetComponent<MeshFilter>();
    }
    
    [ContextMenu("Calculate Convex")]
    public void CalculateConvex()
    {
        Vector3[] verts = mfilter.sharedMesh.vertices;
        foreach (Vector3 v in verts) Debug.Log(v.ToString("F7"));
    }

    [ContextMenu("Calculate and Visualize")]
    public void CalcAndVis()
    {
        // List all triangles
        int[] tris = mfilter.sharedMesh.triangles;
        Vertex[] verts = BuildVertexArray(mfilter.sharedMesh.vertices);
        // Generate all Half-Edges, faces and vertices from the mesh
        for(int f = 0; f < tris.Length; f += 3)
        {
            // Each 3-set of verts is a triangle, so loop through 3 each time and add them to a new facet
        
            // Generate the half-edges

        }

        // Print them
    }

    Vertex[] BuildVertexArray(Vector3[] vertPos)
    {
        Vertex[] vertices = new Vertex[vertPos.Count()];
        for(int i = 0; i <  vertPos.Length; i++)
        {
            vertices[i] = new Vertex(vertPos[i], i);
        }
        return vertices.ToArray();
    }
}
